USE RYAccountsDB
GO
DECLARE @CountBegin INT
DECLARE @CountEnd INT
DECLARE @Name NVARCHAR(20)
DECLARE @FirstName NVARCHAR(20)
DECLARE @MemberOrder INT
SET @CountBegin=1

/*------------------------编辑区域开始----------------------------------------*/
SET @CountEnd=10000 --生成账号的结束数
SET @FirstName='游客' --生成账号的前缀
SET @MemberOrder=0 --普通会员=0,Vip1=1,Vip2=2,Vip3=3,Vip4=4,Vip5=5
/*------------------------编辑区域结束----------------------------------------*/

WHILE(@CountBegin <= @CountEnd)

BEGIN
SET @Name=@FirstName+LTRIM(STR(@CountBegin))
INSERT INTO RYAccountsdb..AccountsInfo (Accounts,NickName,RegAccounts,LogonPass,SpreaderID,PassPortID,Compellation,Gender,FaceID,RegisterIP,RegisterMachine,LastLogonIP,IsAndroid,MemberOrder) VALUES (@Name,@Name,@Name,N'',N'',N'',N'',RAND()*2,RAND()*200,'','','','1',@MemberOrder)
SET @CountBegin=@CountBegin+1
END

UPDATE AccountsInfo SET AccountsInfo.GameID=GameIdentifier.GameID FROM AccountsInfo,GameIdentifier WHERE AccountsInfo.UserID NOT IN (SELECT UserID FROM RYAccountsDB..AndroidLockInfo) AND IsAndroid=1 AND AccountsInfo.UserID=GameIdentifier.UserID
INSERT INTO AndroidLockInfo(UserID,MemberOrder) 
SELECT UserID,MemberOrder FROM AccountsInfo WHERE IsAndroid=1 AND UserID NOT IN (SELECT UserID FROM AndroidLockInfo)
GO