USE RYTreasureDB

--列表配置 金币
TRUNCATE TABLE GameColumnItem

insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (1,'昵称',100,3)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (2,'ID',60,1)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (3,'游戏币',60,30)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (4,'桌号',40,20)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (5,'级别',85,44)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (6,'经验值',85,33)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (7,'魅力值',85,34)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (8,'元宝',60,32)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (9,'胜率',60,40)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (10,'逃率',60,43)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (11,'总局',60,39)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (12,'赢局',60,35)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (13,'输局',60,36)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (14,'和局',60,37)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (15,'逃局',60,38)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (16,'社团',85,11)
insert GameColumnItem (SortID,ColumnName,ColumnWidth,DataDescribe) VALUES (17,'签名',150,12)