USE RYPlatformDB

--类型配置
TRUNCATE TABLE GameTypeItem

insert GameTypeItem (TypeID,JoinID,SortID,TypeName,Nullity) values (1,0,1,'棋牌游戏',0)
insert GameTypeItem (TypeID,JoinID,SortID,TypeName,Nullity) values (2,0,2,'财富游戏',0)
insert GameTypeItem (TypeID,JoinID,SortID,TypeName,Nullity) values (3,0,3,'休闲游戏',0)
insert GameTypeItem (TypeID,JoinID,SortID,TypeName,Nullity) values (4,0,4,'积分游戏',0)

--模块配置
TRUNCATE TABLE GameGameItem

insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (6,'诈金花',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'ZhaJinHuaServer.dll','ZhaJinHua.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (27,'新牛牛',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'OxNewServer.dll','OxNew.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (49,'十三水',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'ThirteenServer.dll','Thirteen.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (200,'斗地主',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'LandServer.dll','Land.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (388,'福州麻将',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'SparrowFZServer.dll','SparrowFZ.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (389,'红中麻将',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'SparrowHZServer.dll','SparrowHZ.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (390,'血战麻将',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'SparrowXZServer.dll','SparrowXZ.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (391,'广东麻将',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'SparrowGDServer.dll','SparrowGD.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (104,'百人牛牛',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'OxBattleServer.dll','OxBattle.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (118,'百人骰宝',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'SicBoBattleServer.dll','SicBoBattle.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (122,'新百家乐',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'BaccaratNewServer.dll','BaccaratNew.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (123,'飞禽走兽',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'AnimalBattleServer.dll','AnimalBattle.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (127,'金鲨银鲨',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'SharkBattleServer.dll','SharkBattle.exe')
insert GameGameItem (GameID,GameName,SuportType,DataBaseAddr,DataBaseName,ServerVersion,ClientVersion,ServerDllName,ClientExeName) values (140,'豪车俱乐部',31,'127.0.0.1','RYTreasureDB',101122049,101122049,'LuxuryCarServer.dll','LuxuryCar.exe')

--电脑游戏配置
TRUNCATE TABLE GameKindItem

insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (6,6,1,0,0,'诈金花','ZhaJinHua.exe','/GameRules.aspx?KindID=6','/Download/ZhaJinHua.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (27,27,1,0,0,'新牛牛','OxNew.exe','/GameRules.aspx?KindID=27','/Download/OxNew.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (49,49,1,0,0,'十三水','Thirteen.exe','/GameRules.aspx?KindID=49','/Download/Thirteen.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (200,200,1,0,0,'斗地主','Land.exe','/GameRules.aspx?KindID=200','/Download/Land.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (388,388,1,0,0,'福州麻将','SparrowFZ.exe','/GameRules.aspx?KindID=388','/Download/SparrowFZ.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (389,389,1,0,0,'红中麻将','SparrowHZ.exe','/GameRules.aspx?KindID=389','/Download/SparrowHZ.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (390,390,1,0,0,'血战麻将','SparrowXZ.exe','/GameRules.aspx?KindID=390','/Download/SparrowXZ.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (391,391,1,0,0,'广东麻将','SparrowGD.exe','/GameRules.aspx?KindID=391','/Download/SparrowGD.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (104,104,2,0,0,'百人牛牛','OxBattle.exe','/GameRules.aspx?KindID=104','/Download/OxBattle.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (118,118,2,0,0,'百人骰宝','SicBoBattle.exe','/GameRules.aspx?KindID=118','/Download/SicBoBattle.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (122,122,2,0,0,'新百家乐','BaccaratNew.exe','/GameRules.aspx?KindID=122','/Download/BaccaratNew.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (123,123,2,0,0,'飞禽走兽','AnimalBattle.exe','/GameRules.aspx?KindID=123','/Download/AnimalBattle.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (127,127,2,0,0,'金鲨银鲨','SharkBattle.exe','/GameRules.aspx?KindID=127','/Download/SharkBattle.exe',1,15,1)
insert GameKindItem (KindID,GameID,TypeID,JoinID,SortID,KindName,ProcessName,GameRuleUrl,DownLoadUrl,Recommend,GameFlag,Nullity) values (140,140,2,0,0,'豪车俱乐部','LuxuryCar.exe','/GameRules.aspx?KindID=140','/Download/LuxuryCar.exe',1,15,1)

--手机游戏配置
TRUNCATE TABLE MobileKindItem

insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (6,'诈金花',0,'QiPai.ZhaJinHua',101122049,0,0,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (27,'新牛牛',0,'QiPai.OxNew',101122049,0,0,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (49,'十三水',0,'QiPai.Thirteen',101122049,0,0,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (200,'斗地主',0,'QiPai.Land',101122049,0,0,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (388,'福州麻将',0,'QiPai.SparrowFZ',101122049,0,0,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (389,'红中麻将',0,'QiPai.SparrowHZ',101122049,0,0,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (390,'血战麻将',0,'QiPai.SparrowXZ',101122049,0,0,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (391,'广东麻将',0,'QiPai.SparrowGD',101122049,0,0,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (104,'百人牛牛',0,'CaiFu.OxBattle',101122049,0,1,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (118,'百人骰宝',0,'CaiFu.SicboBattle',101122049,0,1,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (122,'新百家乐',0,'CaiFu.BaccaratNew',101122049,0,1,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (123,'飞禽走兽',0,'CaiFu.AnimalBattle',101122049,0,1,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (127,'金鲨银鲨',0,'CaiFu.SharkBattle',101122049,0,1,3,1)
insert MobileKindItem (KindID,KindName,TypeID,ModuleName,ClientVersion,ResVersion,SortID,KindMark,Nullity) values (140,'豪车俱乐部',0,'CaiFu.LuxuryCar',101122049,0,1,3,1)
