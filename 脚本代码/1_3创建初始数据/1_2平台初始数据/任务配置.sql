USE RYPlatformDB

--任务配置
TRUNCATE TABLE TaskInfo

SET IDENTITY_INSERT TaskInfo ON

insert TaskInfo (TaskID,TaskName,TaskDescription,TaskType,UserType,KindID,MatchID,Innings,StandardAwardGold,StandardAwardMedal,MemberAwardGold,MemberAwardMedal,TimeLimit,InputDate) values (1,N'斗地主首胜任务',N'斗地主首胜，即可完成任务',4,3,200,0,1,2000,1,4000,2,1800,'2016-01-01 00:00:00.000')
insert TaskInfo (TaskID,TaskName,TaskDescription,TaskType,UserType,KindID,MatchID,Innings,StandardAwardGold,StandardAwardMedal,MemberAwardGold,MemberAwardMedal,TimeLimit,InputDate) values (2,N'斗地主1局任务',N'斗地主1局，即可完成任务',2,3,200,0,1,1000,1,2000,2,1800,'2016-01-01 00:00:00.000')
insert TaskInfo (TaskID,TaskName,TaskDescription,TaskType,UserType,KindID,MatchID,Innings,StandardAwardGold,StandardAwardMedal,MemberAwardGold,MemberAwardMedal,TimeLimit,InputDate) values (3,N'斗地主赢3局任务',N'斗地主赢3局，即可完成任务',1,3,200,0,3,5000,1,10000,2,1800,'2016-01-01 00:00:00.000')
insert TaskInfo (TaskID,TaskName,TaskDescription,TaskType,UserType,KindID,MatchID,Innings,StandardAwardGold,StandardAwardMedal,MemberAwardGold,MemberAwardMedal,TimeLimit,InputDate) values (4,N'斗地主连赢9局任务',N'斗地主连赢9局，即可完成任务',8,3,200,0,9,10000,1,20000,2,1800,'2016-01-01 00:00:00.000')

SET IDENTITY_INSERT TaskInfo OFF