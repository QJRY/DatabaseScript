USE RYTreasureDB

--实卡类型
TRUNCATE TABLE GlobalLivcard

INSERT INTO GlobalLivcard (CardName,CardPrice,Currency,InputDate) VALUES (N'10元卡',10.00,10.00,'2013-10-16 00:00:00.000')
INSERT INTO GlobalLivcard (CardName,CardPrice,Currency,InputDate) VALUES (N'30元卡',30.00,30.00,'2013-10-16 00:00:00.000')
INSERT INTO GlobalLivcard (CardName,CardPrice,Currency,InputDate) VALUES (N'60元卡',60.00,60.00,'2013-10-16 00:00:00.000')
INSERT INTO GlobalLivcard (CardName,CardPrice,Currency,InputDate) VALUES (N'120元卡',120.00,120.00,'2013-10-16 00:00:00.000')
INSERT INTO GlobalLivcard (CardName,CardPrice,Currency,InputDate) VALUES (N'240元卡',240.00,240.00,'2013-10-16 00:00:00.000')