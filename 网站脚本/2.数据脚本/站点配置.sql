USE RYNativeWebDB

--վ������
TRUNCATE TABLE ConfigInfo

SET IDENTITY_INSERT ConfigInfo ON

INSERT ConfigInfo (ConfigID,ConfigKey,ConfigName,ConfigString,Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,SortID) VALUES (1,N'ContactConfig',N'��ϵ��ʽ����',N'����˵��
�ֶ�1:�ͷ��绰
�ֶ�2:�������
�ֶ�3:�ʼ���ַ
�ֶ�4:��ҵQQ',N'400-000-0000',N'0000-88888888',N'test@kxsk.com',N'4000000000',N'',N'',N'',N'',5)
INSERT ConfigInfo (ConfigID,ConfigKey,ConfigName,ConfigString,Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,SortID) VALUES (2,N'SiteConfig',N'վ������',N'����˵��
�ֶ�1:վ������
�ֶ�2:վ������
�ֶ�3:ͼƬ����(�޸ĺ�30��������Ч)
�ֶ�8:��վ�ײ�����',N'��ˣ���ơ�������Ϸƽ̨',N'http://www.sdp.kxsk.com',N'http://wm.sdp.kxsk.com/upload/',N'',N'',N'',N'',N'Copyright @ 2016 kxsk.com All Rights Reserved.��Ȩ���� ������Ѷʱ������Ƽ����޹�˾',1)
INSERT ConfigInfo (ConfigID,ConfigKey,ConfigName,ConfigString,Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,SortID) VALUES (3,N'GameFullPackageConfig',N'������������',N'�ֶ�1:���ص�ַ
',N'/Download/FullGamePlaza.exe',N'',N'',N'',N'',N'',N'',N'',20)
INSERT ConfigInfo (ConfigID,ConfigKey,ConfigName,ConfigString,Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,SortID) VALUES (4,N'GameJanePackageConfig',N'�����������',N'�ֶ�1:���ص�ַ
',N'/Download/MiniGamePlaza.exe',N'',N'',N'',N'',N'',N'',N'',15)
INSERT ConfigInfo (ConfigID,ConfigKey,ConfigName,ConfigString,Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,SortID) VALUES (5,N'EmailConfig',N'��������',N'�ֶ�1:�����˺�
�ֶ�2:��������
�ֶ�3:SmtpServer�����ṩ��ַsmtp.qq.com�ֶ�4:�˿�',N'test@kxsk.com',N'test',N'smtp.qq.com',N'25',N'',N'',N'',N'',30)
INSERT ConfigInfo (ConfigID,ConfigKey,ConfigName,ConfigString,Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,SortID) VALUES (6,N'GameAndroidConfig',N'��׿��������',N'����˵��
�ֶ�1:���ص�ַ
�ֶ�2:�����汾��
�ֶ�3:������ǿ�Ƹ��� 1:�� 0:��',N'http://fir.im/asdp',N'V1.0',N'0',N'',N'',N'',N'',N'',20)
INSERT ConfigInfo (ConfigID,ConfigKey,ConfigName,ConfigString,Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,SortID) VALUES (7,N'GameIosConfig',N'ƻ����������',N'����˵��
�ֶ�1:���ص�ַ
�ֶ�2:�����汾��
�ֶ�3:������ǿ�Ƹ��� 1:�� 0:��',N'http://fir.im/isdp',N'V1.0',N'0',N'',N'',N'',N'',N'',15)
INSERT ConfigInfo (ConfigID,ConfigKey,ConfigName,ConfigString,Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,SortID) VALUES (8,N'MobilePlatformVersion',N'�ƶ����������',N'����˵��
�ֶ�1:����·��
�ֶ�2:�����汾��
�ֶ�3:��Դ�汾��',N'http://www.sdp.kxsk.com/download/phone/',N'0',N'0',N'',N'',N'',N'',N'',0)
INSERT ConfigInfo (ConfigID,ConfigKey,ConfigName,ConfigString,Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,SortID) VALUES (9,N'DayTaskConfig',N'�ֻ�ÿ�ձ���',N'����˵��
�ֶ�1:ǩ��(0:��ʾ,1:����)
�ֶ�2:ת��(0:��ʾ,1:����)
�ֶ�3:�ͱ�(0:��ʾ,1:����)
�ֶ�4:ÿ���׳�(0:��ʾ,1:����)
�ֶ�5:��������(0:��ʾ,1:����)
�ֶ�6:ÿ������(0:��ʾ,1:����)',N'0',N'0',N'0',N'0',N'0',N'0',N'',N'',40)

SET IDENTITY_INSERT ConfigInfo OFF